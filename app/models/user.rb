# encoding: utf-8
class User < ActiveRecord::Base

  #---
  def self.find_or_get(mid)
    user = User.find_by(mid: mid)
    unless user
      user = User.new(mid: mid)
      user.line_profile
    end
    user
  end

  #---
  def conversation(message)
    result = docomo_sentence(message, mid)
    case result[:dialogStatus][:command][:commandId]
    when "BC00101" # 雑談
      res = docomo_dialog(message, mid)
      line_send_message(res[:utt])
    when "BK00101" # 知識検索
      res = docomo_qa(message, mid)
      if res[:code].match(/^E/) then
        line_send_message(Settings.replies.unknown.sample)
      else
        line_send_message(res[:answers][0][:answerText])
      end
    when "BT00101" # 乗換案内
      line_send_message(Settings.replies.transport.sample)
    when "BT00201" # 地図
      line_send_message(Settings.replies.location.sample)
    when "BM00101" # 乗換案内 or 地図
      line_send_message(Settings.replies.transport.sample)
    else
      line_send_message(Settings.replies.unknown.sample)
    end
  end

  #---
  def line_send_message(message)
    f = faraday('https://trialbot-api.line.me')
    replay = {
      to: [mid],
      toChannel: 1383378250, # Fixed  value
      eventType: "138311608800106203", # Fixed value
      content: {
        contentType: 1,
        toType: 1,
        text: message
      }
    }
    res = f.post do |req|
      req.url '/v1/events'
      line_headers(req)
      req.body = replay.to_json
    end
    throw res.body if res.status != 200
  end

  private

  #---
  def line_profile
    f = faraday('https://trialbot-api.line.me')
    res = f.get do |req|
      req.url '/v1/profiles?mids='+mid
      line_headers(req)
    end
    throw res.body if res.status != 200
    line_profile = JSON.parse(res.body).with_indifferent_access
    logger.debug line_profile
    nickname = line_profile[:displayName]
  end

  #---
  def line_headers(req)
    req.headers['Content-Type'] = 'application/json; charser=UTF-8'
    req.headers['X-Line-ChannelID'] = Settings.line[:channel_id]
    req.headers['X-Line-ChannelSecret'] = Settings.line[:channel_secret]
    req.headers['X-Line-Trusted-User-With-ACL'] = Settings.line[:mid]
  end

  #---
  def docomo_sentence(message, mid)
    f = faraday('https://api.apigw.smt.docomo.ne.jp')
    opt = {
      projectKey: 'OSU',
      appInfo: {
        appKey: 'boccaroid'
      },
      clientVer: '1.0.0',
      language: 'ja',
      userId: mid,
      userUtterance: {
        utteranceText: message
      }
    }
    res = f.post do |req|
      params = {
        APIKEY: Settings.docomo[:api_key]
      }
      req.url "/sentenceUnderstanding/v1/task?#{params.to_query}"
      req.headers['Content-Type'] = 'application/x-www-form-urlencoded'
      req.body = {json: opt.to_json}
    end
    throw res.body if res.status != 200
    JSON.parse(res.body).with_indifferent_access
  end

  #---
  def docomo_dialog(message, mid)
    f = faraday('https://api.apigw.smt.docomo.ne.jp')
    opt = {
      utt: message,
      nickname: display_name,
      context: mid
    }
    res = f.post do |req|
      params = {
        APIKEY: Settings.docomo[:api_key]
      }
      req.url "/dialogue/v1/dialogue?#{params.to_query}"
      req.headers['Content-Type'] = 'application/json'
      req.body = opt.to_json
    end
    throw res.body if res.status != 200
    JSON.parse(res.body).with_indifferent_access
  end

  #---
  def docomo_qa(message, mid)
    f = faraday('https://api.apigw.smt.docomo.ne.jp')
    res = f.get do |req|
      params = {
        APIKEY: Settings.docomo[:api_key],
        q: message
      }
      req.url "/knowledgeQA/v1/ask?#{params.to_query}"
      req.headers['Content-Type'] = 'application/json'
    end
    throw res.body if res.status != 200
    JSON.parse(res.body).with_indifferent_access
  end

  #---
  def faraday(url)
    Faraday.new(:url => url) do |faraday|
      faraday.request  :url_encoded
      faraday.response :encoding
      faraday.response :logger, ::Logger.new(STDOUT), bodies: true
      faraday.adapter  Faraday.default_adapter
    end
  end

end
