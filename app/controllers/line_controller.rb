class LineController < ApplicationController
  protect_from_forgery except: :callback

  def callback

    param = JSON.parse(request.body.read, {:symbolize_names => true})
    param = param.with_indifferent_access
    logging_param(param)

    param[:result].each do |msg|
      logger.debug msg
      user = User.find_or_get(msg[:content][:from])
      if msg[:content][:opType] then
        user.status = msg[:content][:opType].to_s
        user.save!
      else
        case msg[:content][:contentType]
        when 1 # text
          user.conversation(msg[:content][:text])
        when 2 # image
          user.line_send_message(Settings.replies.photo.sample)
        when 3 # video
          user.line_send_message(Settings.replies.video.sample)
        when 4 # audio
          user.line_send_message(Settings.replies.audio.sample)
        when 7 # location
          user.line_send_message(Settings.replies.location.sample)
        when 8 # sticker
          user.line_send_message(Settings.replies.sticker.sample)
        when 10 # contact message
          user.line_send_message(Settings.replies.contact.sample)
        else #起こりえない
          user.line_send_message(Settings.replies.unknown.sample)
        end
      end
    end
  end

  private
  def logging_param(param)
    File.open("log/callback.txt", "a") do |file|
      file.write(JSON.pretty_generate(param))
      file.write("\n---\n")
    end
  end
end
