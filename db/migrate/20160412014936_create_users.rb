class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :mid
      t.string :display_name
      t.string :picture_url
      t.string :status_message
      t.string :stauts

      t.timestamps null: false
    end
    add_index :users, :mid, unique: true
  end
end
