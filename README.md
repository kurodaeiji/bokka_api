# Boccaa - Line Bot

![Bocca](https://bitbucket.org/repo/azoqgX/images/3417005623-bocca250.png)

Bokka is a line bot which is using [docomo dialog api](https://dev.smt.docomo.ne.jp/?p=docs.api.page&api_name=dialogue&p_name=api_1) for dialog.

Please add to your friends.

![ksg1704i.png](https://bitbucket.org/repo/azoqgX/images/768829745-ksg1704i.png)

## Build

You need to get [docomo api keys](https://dev.smt.docomo.ne.jp/?p=docs.api.page&api_name=dialogue&p_name=api_usage_scenario) and [Line api keys](https://developers.line.me/bot-api/overview).

and you need to make `config/settings.local.yml` and also `config/secrets.yml`.